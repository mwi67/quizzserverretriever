#!/usr/bin/env python3

import base64
import inspect
import os

from bs4 import BeautifulSoup as bs
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from seleniumrequests import Firefox
from selenium.webdriver.firefox.options import Options

import credentials
from HTMLHelper import html_header
from HTMLHelper import html_footer


def logger(func):
    def wrap(*args, **kwargs):
        func_args = inspect.signature(func).bind(*args, **kwargs).arguments
        func_args_str = ', '.join('{} = {!r}'.format(*item)
                                  for item in func_args.items())
        print(f'{func.__module__}.{func.__qualname__} ( {func_args_str} )')
        return func(*args, **kwargs)

    return wrap


class Scraper:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.root_url = "https://quiz2019.csse.canterbury.ac.nz"
        self.courses = {}
        self.enrolled_courses = {}

        options = Options()
        options.headless = True
        self.driver = Firefox(options=options)

        self.delay = 1

        self.root_dir = "scraped/"
        self.images_dir = "{}/img/".format(self.root_dir)

        if not os.path.exists(self.root_dir):
            print("Creating directory {}".format(self.root_dir))
            os.makedirs(self.root_dir)

    def login(self):
        self.driver.get("{}/login/index.php".format(self.root_url))
        username = self.driver.find_element_by_id("username")
        username.send_keys(credentials.username)
        password = self.driver.find_element_by_id("password")
        password.send_keys(credentials.password)

        login_btn = self.driver.find_element_by_id("loginbtn")
        login_btn.click()

    def _find_all_courses(self):
        # Go into quiz server 2019 site home
        self.driver.get("{}/?redirect=0".format(self.root_url))

        soup = bs(self.driver.page_source, "html.parser")

        # From 'h3' tag, get 'a' tag with href and text, and put them in variables link and course
        for a in soup.findAll('h3'):
            link = a.find('a', href=True)
            course = a.text

            # Get url link from 'a' tag and save it to variable 'url' and make a dictionary
            if link is not None and course is not None:
                url = link['href']

                self.courses[course] = url

    def _is_restricted_course(self, course_page_text):
        return 'Enrol me' in course_page_text \
            or 'Enrolment is disabled' in course_page_text \
            or 'enrolment ended' in course_page_text

    def find_enrolled_courses(self):
        # Create a dictionary of courses that I am enrolled in
        self._find_all_courses()

        for course, url in self.courses.items():

            course_site = self.driver.request("GET", url)
            course_site_text = course_site.text

            if self._is_restricted_course(course_site_text):
                continue

            self.enrolled_courses[course] = url

    def _is_irrelevant_activity(self, activity):
        quiz_link = activity.find('a', href=True)    # Link to activity

        if "announcements" in quiz_link.text.lower() \
                or "practice copy" in quiz_link.text.lower():
            return True

        img_tag = activity.find('img')
        src_url = img_tag['src']

        if 'pdf' in src_url or \
                'archive' in src_url or \
                'forum' in src_url:
            return True

        return False

    def find_activities(self, enrolled_course):
        course, url = enrolled_course

        self.driver.get(url)
        quiz_soup = bs(self.driver.page_source, 'html.parser')

        return quiz_soup.findAll('div', {'class': 'activityinstance'})

    def get_activity_name(self, activity):
        return activity.text.strip()

    def find_review_links_in_activity(self, activity):
        review_link = ""
        quiz_link = activity.find('a', href=True)    # Link to activity

        if self._is_irrelevant_activity(activity):
            return ""

        # Get into each lab quizzes
        href_quiz_link = quiz_link['href']
        self.driver.get(href_quiz_link)

        bs_quiz_entrance = bs(self.driver.page_source, "html.parser")

        a_tag = bs_quiz_entrance.find(
            'a', {'title': "Review your responses to this attempt"}, href=True)

        if a_tag is not None:
            review_link = a_tag['href']

        return review_link

    def _replace_img_urls(self, page_soup):
        for img in page_soup.findAll("img"):
            original_url = img["src"]
            modified_url = "img/" + self._get_filename(original_url)
            img["src"] = modified_url

        return page_soup

    def _remove_domain_name(self, full_url):
        url_split = full_url.split('/')
        for i in range(len(url_split)):
            url = url_split[i]
            url = url.split('?')[0]
            url_split[i] = url
        modified_domain = '/'.join(url_split[3:])

        return modified_domain

    def _download_and_modify_script_urls(self, page_soup):
        link_urls = page_soup.findAll("link", href=True)
        script_srcs = page_soup.findAll("script", src=True)
        all_urls = link_urls + script_srcs

        for url in all_urls:
            src = ""
            if url.name == "script":
                src = url["src"]
            else:
                src = url["href"]

            clean_url = self._remove_domain_name(src)

            if url.name == "script":
                url["src"] = clean_url
            else:
                url["href"] = clean_url

            if src.startswith("//"):
                src = "http:{}".format(src)
            response = self.driver.request("GET", src)
            filepath = "{}/{}".format(self.root_dir, clean_url)
            self._write_to_file(response, filepath)

        return page_soup

    def _remove_name_traces(self, page_soup):
        username_on_pagetop = page_soup.find("span", {"class": "userbutton"})
        username_on_pagetop.decompose()

        username_on_pagebottom = page_soup.find("div", {"class": "logininfo"})
        username_on_pagebottom.decompose()

        return page_soup

    def _remove_summary_table(self, page_soup):
        summary_table = page_soup.find("table", {"class": "quizreviewsummary"})
        summary_table.decompose()

        return page_soup

    def parse_review_link(self, review_link):
        self.driver.get(review_link)
        quiz_page_src = self.driver.page_source

        page_soup = bs(quiz_page_src, "html.parser")

        self._find_images_in_page(page_soup)

        page_soup = self._replace_img_urls(page_soup)
        page_soup = self._remove_name_traces(page_soup)
        page_soup = self._remove_summary_table(page_soup)
        page_soup = self._download_and_modify_script_urls(page_soup)

        modified_src = str(page_soup)

        return modified_src

    def _get_filename(self, url):
        clean_filename = url.split('/')[-1]
        if '?' in clean_filename:
            clean_filename = clean_filename.split('?')[0]
        return clean_filename

    def _write_to_file(self, response, filename):
        dirname = filename.split('/')[:-1]
        dirname = '/'.join(dirname)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        with open(filename, 'wb') as f:
            for chunk in response.iter_content(chunk_size=128):
                if chunk:
                    f.write(chunk)

    def _is_valid_url(self, url):
        return url.startswith("http")

    def _find_images_in_page(self, page_soup):
        images = page_soup.findAll("img", src=True)
        for img in images:
            image_url = img["src"]
            filename = self._get_filename(image_url)
            download_path = "{}/{}".format(self.images_dir, filename)
            if os.path.exists(download_path):
                continue

            if not self._is_valid_url(image_url):
                continue

            response = self.driver.request("GET", image_url)
            if not os.path.exists(download_path):
                self._write_to_file(response, download_path)

    def close(self):
        self.driver.close()


def main():
    scraper = Scraper(credentials.username, credentials.password)
    scraper.login()
    scraper.find_enrolled_courses()

    root_dir = scraper.root_dir

    with open("{}/index.html".format(root_dir), "w") as f:
        f.write(html_header())

        # Put contents here

        f.write("\n")

        courses = scraper.enrolled_courses

        for enrolled_course in courses.items():
            course, url = enrolled_course

            #if not course.startswith("COSC261"):
            #    continue

            template = ""
            course_filename = course.split(" ")[0] + ".html"
            template += "# [{}]({})\n\n".format(course, course_filename)
            f.write(template)

            activities = scraper.find_activities(enrolled_course)
            with open("{}/{}".format(root_dir, course_filename),
                      "w") as course_file:
                course_file.write(html_header())
                course_file.write("## {}\n\n".format(course))
                for activity in activities:
                    review_link = scraper.find_review_links_in_activity(
                        activity)
                    if review_link != "":
                        activity_filename = activity.text.replace(
                            " ", "_") + ".html"
                        course_file.write(
                            "### <a href=\"./{}\">{}</a>\n\n".format(
                                activity_filename,
                                scraper.get_activity_name(activity)))
                        line = scraper.parse_review_link(review_link)
                        with open("{}/{}".format(root_dir, activity_filename),
                                  "w") as c_file:
                            c_file.write(line)

                course_file.write(html_footer())

        f.write(html_footer())

    scraper.close()


if __name__ == "__main__":
    main()
