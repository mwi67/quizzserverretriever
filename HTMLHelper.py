#!/usr/bin/env python3


def html_header():
    return '''
<!DOCTYPE html>
<html>
<title>Past exercise</title>
<xmp theme="united" style="display:none;">
'''


def html_footer():
    return '''
</xmp>
<script type="text/x-mathjax-config">MathJax.Hub.Config({ 
jax: ["input/TeX","output/HTML-CSS"],
displayAlign: "left",
    tex2jax: { inlineMath: [['$','$']], displayMath: [['$$', '$$']], processEscapes: true } });</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML&amp;locale=en"></script>

<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>
'''
